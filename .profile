# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export CLICOLOR=1 #Colors in terminal

export LSCOLORS=ExFxCxDxBxegedabagacad #Color scheme for terminal ( you can change this if you don't like the color$

#export PATH=/usr/local/Cellar/php70/7.0.9/bin/:/Users/stefan/pear/bin:$PATH #Sets PHP7 as your default php CLI bin$

#source /Library/Developer/CommandLineTools/usr/share/git-core/git-completion.bash #Adds completion to the git comm$

#source /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh #Allows the prompt to include the git $

GIT_PS1_SHOWDIRTYSTATE=true #Shows the state of the repo(dirty if there are uncommitted changes)

GIT_PS1_SHOWCOLORHINTS=true #SHows colors in git status display and diff

PS1='\a\n\n\e[31;1m\u@\h on \d at \@\n\e[33;1m\w\e[0m$(__git_ps1 " (%s)")\n$ ' #Prompt sequence( user@localhost, da$

export ANDROID_HOME=${HOME}/Android/Sdk 
export PATH=${PATH}:${ANDROID_HOME}/tools 
export PATH=${PATH}:${ANDROID_HOME}/platform-tools
